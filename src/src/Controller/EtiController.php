<?php

namespace App\Controller;

use App\Entity\BlogPost;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EtiController
 */
class EtiController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function blogHomepage()
    {
        return $this->render('eti/blog/homepage.html.twig');
    }

    /**
     * @Route("/first/page", name="first_page")
     * @param TranslatorInterface $translator
     * @return Response
     * @throws Exception
     */
    public function randomNumber(TranslatorInterface $translator)
    {
        $number = random_int(0, 100);

        return $this->render('eti/blog/first_page.html.twig', [
            'number' => $number,
            'translated_php' => $translator->trans('Translated string'),
            'translated_php_pl' => $translator->trans('Translated string', [], 'messages', 'pl_PL')
        ]);
    }


    /**
     * @Route("posts/list", name="post_listing")
     *
     * @return Response
     * @throws Exception
     */
    public function listBlogPosts()
    {
        $repository = $this->getDoctrine()->getRepository(BlogPost::class);
        $articles = $repository->findAll();

        //TODO: add filter by is_visible flag

        return $this->render('eti/blog/posts.html.twig', [
            'articles' => $articles
        ]);
    }


    /**
     * @Route("posts/view", name="post_details")
     *
     * @return Response
     * @throws Exception
     */
    public function postDetails()
    {
        //TODO: get blog post by its $id and return to the template

        $article = null;
        return $this->render('eti/blog/post_view.html.twig', [
            'articles' => $article
        ]);
    }
}